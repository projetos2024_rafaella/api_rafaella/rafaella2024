const router = require('express').Router()

const teacherController = require('../controller/teacherController')

const alunoController = require('../controller/alunoController')

const JSONPlaceholderController = require('../controller/JSONPlaceholderController')

router.get('/teacher/', teacherController.getTeacher)

router.post('/cadastroaluno/', alunoController.postCadastroaluno)

router.put('/cadastroaluno/', alunoController.updateAluno)

router.delete('/cadastroaluno/:id', alunoController.deleteAluno)

router.get("/external/", JSONPlaceholderController.getUsers)

router.get("/external/io",  JSONPlaceholderController.getWebSiteIO)

router.get("/external/com",  JSONPlaceholderController.getWebSiteCOM)

router.get("/external/net",  JSONPlaceholderController.getWebsiteNET)

module.exports = router;