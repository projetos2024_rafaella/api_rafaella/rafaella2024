const axios = require ("axios");

module.exports = class JSONPlaceholderController {
    static async getUsers(req, res){
        try{
        const response =  await axios.get("https://jsonplaceholder.typicode.com/users")
        const users =response.data;
        res.status(200).json({message: 'Aqui estão os usários captados da Api pública JSONPlaceholder', users})
        }catch(error){
            console.error(error);
            res.status(500).json({error: "Falha ao encontrar usuários"});
        }
    }

    static async getWebSiteIO(req, res) {
        try{
            const response = await axios.get("https://jsonplaceholder.typicode.com/users");
            const users = response.data.filter(
                (user) => user.website.endsWith(".io")
            )
            const banana = users.lenght
            res.status(200)
            .json({
                message:
                 "AQUI ESTÃO OS USUÁRIOS COM DOMÍNIO .IO",
                  users, banana
            });
        }
        catch(error){
            console.log(error)
            res.status(500).json({error:"Deu ruim,error"})
        }
    }

    static async getWebSiteCOM(req, res){
        try{
            const response = await axios.get("https://jsonplaceholder.typicode.com/users");
            const users = response.data.filter(
                (user) => user.website.endsWith(".com")
            )
            const banana = users.lenght
            res.status(200)
            .json({
                message:
                 "Aqui estão os usuários com domínio .com",
                  users, banana
            });
        }
        catch(error){
            console.log(error)
            res.status(500).json({error:"Deu ruim,error"})
        }
    }
    static async getWebsiteNET(req, res){
        try{
            const response = await axios.get("https://jsonplaceholder.typicode.com/users");
            const users = response.data.filter(
                (user) => user.website.endsWith(".net")
            )
            const banana = users.lenght
            res.status(200)
            .json({
                message:
                 "Aqui estão os usuários com domínio .net",
                  users, banana
            });
        }
        catch(error){
            console.log(error)
            res.status(500).json({error:"Deu ruim,error"})
        }
    }
};