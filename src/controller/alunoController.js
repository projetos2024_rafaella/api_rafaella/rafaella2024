module.exports = class alunoController {
  //Cadastrar alunos
  static async postCadastroaluno(req, res) {
    const { nome, idade, profissao, cursoMatriculado } = req.body;
    console.log(nome);
    console.log(idade);
    console.log(profissao);
    console.log(cursoMatriculado);

    return res.status(201).json({ message: "Aluno cadastrado" });
  }
  
 //Update alunos
 static async updateAluno(req, res){
  const { nome, idade, profissao, cursoMatriculado} = req.body;
  if(nome !== "" && idade !== null && profissao !== "" && cursoMatriculado !== ""){
      return res.status(200).json({ message: "Aluno editado"});
  }
  else{
      return res.status(200).json({ message: "Não foi possível editar"});
  }
}

//Delete alunos
static async deleteAluno(req, res){
 const { id } = req.params.id; // Alteração aqui
 if (id !== "") { // Verificando se o id existe
  return res.status(200).json({ message: "Aluno deletado"});
}
else{
  return res.status(200).json({ message: "Não foi possível deletar"});
    }
}
};
